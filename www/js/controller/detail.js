function detailController($scope, $routeParams){
  $scope.initialize = function(){
    console.log('detailController is initialized');
    $scope.title = "Unknown";
    $scope.author = "Unknown";
  };

  $scope.ensureBookInfomation = function(){
    $scope.title = "Anatomy of Expectation";
    $scope.author = "JinGyeong Jeong";
    $scope.ISBN = $routeParams.ISBN;
  };

  $scope.backbutton = function(){
    window.history.back();
  };
}