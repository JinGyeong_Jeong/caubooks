function account($scope){
  $scope.initialize = function(){
    $scope.isLoggedIn = false;
  };
  $scope.login = function(){
    console.log($scope.userId + ", " + $scope.userPassword + " log in request");
    $scope.isLoggedIn = true;
  };

  $scope.logout = function(){
    console.log("logout");
    $scope.isLoggedIn = false;
  };

  $scope.back = function(){
    window.history.back();
  };
}