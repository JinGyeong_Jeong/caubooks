angular.module('main', []).config(['$routeProvider',
  function($routeProvider){
    $routeProvider
      .when('/search', {
        templateUrl: 'search.html',
      })
      .when('/detail/:ISBN', {
        templateUrl: 'detail.html',
      })
      .otherwise({
        redirectTo: 'search'
      });
  }
]);
